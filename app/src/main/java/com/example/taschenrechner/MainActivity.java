package com.example.taschenrechner;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import net.objecthunter.exp4j.ValidationResult;

public class MainActivity extends AppCompatActivity {

    private boolean is_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.is_result = false;
    }

    public void onClick(View view) {
        String input = ((Button) view).getText().toString();
        calcInternals(input);
    }

    @SuppressLint("DefaultLocale")
    private void calcInternals(String input) {
        TextView output = findViewById(R.id.output);
        final String error = "ERROR";
        switch (input) {
            case "=":
                try {
                    Expression e = new ExpressionBuilder(output.getText().toString()).build();
                    ValidationResult val = e.validate();
                    if (val.isValid()) {
                        double result = e.evaluate();
                        output.setText(String.format("%f", result).replace(",","."));
                    } else {
                        output.setText(error);
                    }
                } catch (Exception exc) {
                    output.setText(error);
                } finally {
                    this.is_result = true;
                }
                break;

            case "CE":
                output.setText("");
                this.is_result = false;
                break;
            default:
                if (output.getText().equals(error) | (this.is_result && !"/*-+".contains(input))) {
                    output.setText(input);
                } else {
                    output.setText(String.format("%s%s", output.getText(), input));
                }
                this.is_result = false;
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Configuration conf = getResources().getConfiguration();
        if (conf.orientation == Configuration.ORIENTATION_PORTRAIT || conf.screenLayout == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            getMenuInflater().inflate(R.menu.more_ops, menu);
        }
        // Inflate menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_sine:
            case R.id.menu_cosine:
            case R.id.menu_tangent:
            case R.id.menu_sqrt:
                calcInternals(item.toString());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
